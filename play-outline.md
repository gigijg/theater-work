#Characters
---
- Nataliya Van Niftrik (Russia-born journalist)
- Erik Stafford (Worker for the department of secure communications [DOSC])
- Takara Yamada (Friend of Nataliya)
- Adrian De La Salvador (Agent for the Department of safety and encforcment [DOSE)
- CONGO (Amazon rip-off)
---
#Outline

- Beginning focuses on Takara and Nataliya's life under the oppressive regime.

- Takara is creating the notebook during the story (is shown by him writing in it at times)

- Takara is assasinated at the end.

- Unamed individual takes a copy of Takara's notebook to Nataliya after he dies

- Nataliya reads note attached to notebook (contains summary of tyranical state plan. See notes.md for more detail)

- Nataliya copies notebook and distributes it to the populace, thwarting the state's attempt to supress the information contained in it

See notes.md for ideas regarding enhancments/notebook details
